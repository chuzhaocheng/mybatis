package com.atguigu.cn.mybatis;

import com.atguigu.cn.pojo.User;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.InputStream;

/**
 * @author chuzhaocheng
 * @create 2019/11/21 15:19
 */
public class MyBatisTest {

    public static void main(String[] args) throws IOException {
        //获取全局配置文件输入流
        InputStream inputStream = Resources.getResourceAsStream("mybatis-config.xml");

        //加载全局配置文件
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

        //获取sqlSession
        SqlSession sqlSession = sqlSessionFactory.openSession();

        //第一个参数：名称空间.语句的唯一标识
        //第二个参数：sql语句传递的参数
        User user = sqlSession.selectOne("UserMapper.queryUserById",1L);
        System.out.println(user);
        //释放资源
        sqlSession.close();
    }
}
