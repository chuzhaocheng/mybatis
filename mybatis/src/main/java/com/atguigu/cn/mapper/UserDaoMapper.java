package com.atguigu.cn.mapper;

import com.atguigu.cn.pojo.User;
import org.apache.ibatis.annotations.Param;
import java.util.List;
import java.util.Map;

/**
 * @author chuzhaocheng
 * @create 2019/11/28 14:16
 */
public interface UserDaoMapper {

    /**
     * 根据id获取用户信息
     *
     * @param id
     * @return
     */
    public User queryUserById(@Param("id") Long id);

    /**
     * 查询所有用户
     * @return
     */
    public List<User> queryUserList();

    /**
     * 新增用户
     * @param user
     */
    public void insertUser(User user);

    /**
     * 更新用户信息
     * @param user
     */
    public void updateUser(User user);

    /**
     * 根据id删除用户信息
     * @param id
     */
    public void deleteUserById(Long id);

    /**
     * 用户登录，map参数
     * @param map
     * @return
     */
    public User loginMap(Map<String,String> map);

    /**
     * 用户登录参数为pojo
     * @param user
     * @return
     */
    public User loginUser(User user);
}
