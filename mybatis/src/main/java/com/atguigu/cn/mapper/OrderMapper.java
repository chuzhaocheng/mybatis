package com.atguigu.cn.mapper;

import com.atguigu.cn.pojo.Order;
import com.sun.org.apache.xpath.internal.operations.Or;
import org.apache.ibatis.annotations.Param;

/**
 * @author chuzhaocheng
 * @create 2019/11/28 17:21
 */
public interface OrderMapper {

    public Order queryOrderAndUserByOrderNumber(@Param("orderNumber")String orderNumber);

    public Order queryOrderAndUserAndOrderdetailByOrderNumber(@Param("orderNumber") String orderNumber);

    public Order queryOrderAndUserAndOrderdetailAndItemByOrderNumber(@Param("orderNumber")String orderNumber);
}
