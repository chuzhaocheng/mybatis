package com.atguigu.cn.dao.impl;

import com.atguigu.cn.dao.UserDao;
import com.atguigu.cn.pojo.User;
import org.apache.ibatis.session.SqlSession;

import java.util.List;

/**
 * @author chuzhaocheng
 * @create 2019/11/21 16:03
 */
public class UserDaoImpl implements UserDao {

    private SqlSession sqlSession;

    //空参构造函数
    public UserDaoImpl() {
    }

    //有参构造函数
    public UserDaoImpl(SqlSession sqlSession) {
        this.sqlSession = sqlSession;
    }

    @Override
    public User queryUserById(Long id) {
        User user=sqlSession.selectOne("UserMapper.queryUserById",id);
        return user;
    }

    @Override
    public List<User> queryUserList() {
        List<User> list = sqlSession.selectList("UserMapper.queryUserList");
        return list;
    }


    @Override
    public void insertUser(User user) {
        //返回值是该方法操作的记录数
        int count = sqlSession.insert("UserMapper.insertUser", user);
        System.out.println(count);
        sqlSession.commit();//提交事务
    }

    @Override
    public void updateUser(User user) {
        //返回值是该方法操作的记录数
        int count = sqlSession.update("UserMapper.updateUser", user);
        System.out.println(count);
        sqlSession.commit();//提交事务
    }

    @Override
    public void deleteUserById(Long id) {
        //返回值是该方法操作的记录数
        int count = sqlSession.delete("UserMapper.deleteUserById", id);
        System.out.println(count);
        sqlSession.commit();//提交事务
    }
}
