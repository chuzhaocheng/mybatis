package com.atguigu.cn.dao;

import com.atguigu.cn.pojo.User;

import java.util.List;

/**
 * @author chuzhaocheng
 * @create 2019/11/21 16:02
 */
public interface UserDao {

    /**
     * 根据id获取用户信息
     *
     * @param id
     * @return
     */
    public User queryUserById(Long id);

    /**
     * 查询所有用户
     * @return
     */
    public List<User> queryUserList();

    /**
     * 新增用户
     * @param user
     */
    public void insertUser(User user);

    /**
     * 更新用户信息
     * @param user
     */
    public void updateUser(User user);

    /**
     * 根据id删除用户信息
     * @param id
     */
    public void deleteUserById(Long id);
}
