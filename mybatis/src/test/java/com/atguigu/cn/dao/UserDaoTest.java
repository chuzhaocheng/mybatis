package com.atguigu.cn.dao;

import com.atguigu.cn.dao.impl.UserDaoImpl;
import com.atguigu.cn.pojo.User;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Before;
import org.junit.Test;

import java.io.InputStream;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

/**
 * @author chuzhaocheng
 * @create 2019/11/28 10:35
 */
public class UserDaoTest {

    //提升userDao的作用域
    private UserDao userDao;

    @Before
    public void setUp() throws Exception {
        //获取全局配置文件输入流
        InputStream inputStream = Resources.getResourceAsStream("mybatis-config.xml");

        //加载全局配置文件
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

        //获取sqlSession
        SqlSession sqlSession = sqlSessionFactory.openSession();

        userDao = new UserDaoImpl(sqlSession);

    }

    @Test
    public void queryUserById() {
        User user = userDao.queryUserById(1L);
        System.out.println("user = " + user);
    }

    @Test
    public void queryUserList() {
        List<User> userList = userDao.queryUserList();
        System.out.println("userList = " + userList);
    }

    @Test
    public void insertUser() {
        User user=new User();
        user.setAge(16);
        user.setBirthday(new Date());
        user.setUserName("lixirui");
        user.setSex(1);
        user.setPassword("123456");
        user.setName("李溪芮");
        userDao.insertUser(user);

    }

    @Test
    public void updateUser() {
        User user = userDao.queryUserById(10L);
        user.setPassword("12345");
        userDao.updateUser(user);
    }

    @Test
    public void deleteUserById() {
        userDao.deleteUserById(7L);
    }
}