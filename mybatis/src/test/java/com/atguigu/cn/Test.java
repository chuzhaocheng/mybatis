package com.atguigu.cn;

import java.util.HashMap;
import java.util.Set;

/**
 * @author chuzhaocheng
 * @create 2019/11/27 15:53
 */
public class Test {

    //map的方法添加putAll();方法
    public static void main(String[] args) {

        String keyword="aa";
        StringBuffer qsb = new StringBuffer();
        qsb.append("title:");
        qsb.append(keyword);
        qsb.append(" OR description:");
        qsb.append(keyword);
        System.out.println(qsb.toString());//运行的结果title:aa OR description:aa

        HashMap<String, String> map = new HashMap<>();
        map.put("张三","25");
        map.put("李四","26");
        map.put("王五","27");
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.putAll(map);
        Set<String> keySet = hashMap.keySet();
        for (String s : keySet) {
            System.out.println("key："+s+"value:"+hashMap.get(s));
        }
    }
}
