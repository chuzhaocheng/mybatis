package com.atguigu.cn.mapper;

import com.atguigu.cn.pojo.Order;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Before;
import org.junit.Test;

import java.io.InputStream;

import static org.junit.Assert.*;

/**
 * @author chuzhaocheng
 * @create 2019/11/28 17:45
 */
public class OrderMapperTest {

    private OrderMapper orderMapper;

    @Before
    public void setUp() throws Exception {
        InputStream inputStream = Resources.getResourceAsStream("mybatis-config.xml");
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
        SqlSession sqlSession = sqlSessionFactory.openSession(true);
        //动态代理
        orderMapper = sqlSession.getMapper(OrderMapper.class);

    }

    @Test
    public void queryOrderAndUserByOrderNumber() {
        Order order = orderMapper.queryOrderAndUserByOrderNumber("20140921003");
        System.out.println("order = " + order);
    }

    @Test
    public void queryOrderAndUserAndOrderdetailByOrderNumber(){
        Order order = orderMapper.queryOrderAndUserAndOrderdetailByOrderNumber("20140921001");
        System.out.println("order = " + order);
    }

    @Test
    public void queryOrderAndUserAndOrderdetailAndItemByOrderNumber(){
        Order order = orderMapper.queryOrderAndUserAndOrderdetailAndItemByOrderNumber("20140921001");
        System.out.println("order = " + order);
    }
}