package com.atguigu.cn.mapper;

import com.atguigu.cn.pojo.User;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Before;
import org.junit.Test;


import java.io.InputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import static org.junit.Assert.*;

/**
 * @author chuzhaocheng
 * @create 2019/11/28 14:25
 */
public class UserDaoMapperTest {

    private UserDaoMapper userDaoMapper;

    @Before
    public void setUp() throws Exception {
        InputStream inputStream = Resources.getResourceAsStream("mybatis-config.xml");
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
        SqlSession sqlSession = sqlSessionFactory.openSession(true);
        //动态代理
        userDaoMapper = sqlSession.getMapper(UserDaoMapper.class);


    }

    @Test
    public void queryUserById() {
        User user = userDaoMapper.queryUserById(1L);
        System.out.println("user = " + user);
    }

    @Test
    public void queryUserList() {
        List<User> userList = userDaoMapper.queryUserList();
        System.out.println("userList = " + userList);
    }

    @Test
    public void insertUser() {
        User user=new User();
        user.setAge(17);
        user.setBirthday(new Date());
        user.setUserName("yueyunpeng");
        user.setSex(1);
        user.setPassword("123456");
        user.setName("岳云鹏");
        userDaoMapper.insertUser(user);
    }

    @Test
    public void updateUser() {
        User user = userDaoMapper.queryUserById(11L);
        user.setName("储召成");
        userDaoMapper.updateUser(user);
    }

    @Test
    public void deleteUserById() {
        userDaoMapper.deleteUserById(11L);
    }

    @Test
    public void testLoginMap(){
        HashMap<String, String> map = new HashMap<>();
        map.put("userName","zhangsan");
        map.put("password","123456");
        User user = userDaoMapper.loginMap(map);
        System.out.println("user = " + user);
    }

    @Test
    public void testLoginUser(){
        User user=new User();
        user.setUserName("zhangsan");
        user.setPassword("123456");
        User loginUser = userDaoMapper.loginUser(user);
        System.out.println("loginUser = " + loginUser);
    }
}